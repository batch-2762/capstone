from abc import ABC, abstractmethod


class Person(ABC):

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.requests = []

    def getFullName(self):
        return f"{self.first_name} {self.last_name}"

    def addRequest(self, request):
        self.requests.append(request)

    def checkRequest(self):
        if self.requests:
            return self.requests.pop(0)
        else:
            return None

    @abstractmethod
    def addUser(self):
        pass



# =========================================== #


class Employee(Person):

    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name)
        self.__email = email
        self.__department = department

    # Getter and setter methods for private properties
    def getEmail(self):
        return self.__email

    def setEmail(self, email):
        self.__email = email

    def getDepartment(self):
        return self.__department

    def setDepartment(self, department):
        self.__department = department

    def checkRequest(self):
        return "Checking employee's requests"

    def addUser(self):
        return "Adding a new employee"

    def login(self):
        return f"{self.getEmail()} has logged in"

    def logout(self):
        return f"{self.getEmail()} has logged out"



# ========================================================== #


class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name)
        self.__email = email
        self.__department = department
        self.members = []

    # Getter and setter methods for private properties
    def getEmail(self):
        return self.__email

    def setEmail(self, email):
        self.__email = email

    def getDepartment(self):
        return self.__department

    def setDepartment(self, department):
        self.__department = department

    def checkRequest(self):
        return "Checking team lead's requests"

    def addUser(self):
        return "Adding a new team lead"

    def login(self):
        return f"{self.getEmail()} has logged in"

    def logout(self):
        return f"{self.getEmail()} has logged out"

    def addMember(self, employee):
        self.members.append(employee)


# ================================================== #

class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name)
        self.__email = email
        self.__department = department

    # Getter and setter methods for private properties
    def getEmail(self):
        return self.__email

    def setEmail(self, email):
        self.__email = email

    def getDepartment(self):
        return self.__department

    def setDepartment(self, department):
        self.__department = department

    def checkRequest(self):
        return "Checking admin's requests"

    def addUser(self):
        return "Adding a new user"

    def login(self):
        return f"{self.getEmail()} has logged in"

    def logout(self):
        return f"{self.getEmail()} has logged out"

    def addUser(self):
        return "New user added"


# ================================================= #

class Request:
    def __init__(self, name, requester, date_requested, status):
        self.name = name
        self.requester = requester
        self.date_requested = date_requested
        self.status = status

    def updateRequest(self, new_status):
        self.status = new_status

    def closeRequest(self):
        self.status = "closed"

    def cancelRequest(self):
        self.status = "canceled"






# CAPSTONE TEST CASES
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
reql = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


# CAPSTONE SPECIFICATIONS
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Micheal Specter", "Full name should be Micheal Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"


# CAPSTONE SPECIFICATIONS
teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())